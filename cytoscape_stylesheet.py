text_outline_width = '1px'
text_outline_color = '#4c5052'
stylesheet = [
    {
        'selector': 'node',
        'style': {
            'opacity': 0.8,
            'padding': '5px',
            'text-halign': 'center',
            'text-valign': 'center',
            "text-wrap": "wrap",
            'color': '#fff',
            'font-size': '10px',
            'background-color': '#4c5052',
            'z-index': 5
        },
    },
    {
        'selector': 'edge',
        'style': {
            'width': '1px',
            'curve-style': 'bezier',  # 'unbundled-bezier',
            'line-color': '#DDDDDD',
            'opacity': '0.7',
            'z-index': 1,
            # 'content': 'data(label)',
            # 'color': '#000',
            # 'font-size': '12px',
            # 'text-background-color': '#fff',
            # 'text-background-opacity': 1,
            # 'text-background-padding': '5px',
            # 'text-border-opacity': 1,
            # 'text-border-width': '1px',
            # 'text-border-color': '#aaa',
        }
    },
    {
        'selector': '.project',
        'style': {
            'content': 'data(label)',
            'text-max-width': '90px',
            'width': '80px',
            'height': '80px',
            'text-outline-opacity': 1,
            'text-outline-color': text_outline_color,
            'text-outline-width': text_outline_width,
            # 'shape': 'circle',
            # 'background-color': 'mapData(weight, 0, 1, grey, green)',
        },
    },
    {
        'selector': '.person',
        'style': {
            'content': 'data(label)',
            'text-outline-opacity': 1,
            'text-outline-color': text_outline_color,
            'text-outline-width': text_outline_width,
            # 'text-rotation': 'data(angle)',
            # 'background-color': 'mapData(weight, 0, 1, yellow, orange)',
        }
    },
    {
        'selector': '.section',
        'style': {
            'content': 'data(label)',
            'font-size': '14px',
            # 'margin': '20px 0 0 0',
            'background-color': '#ffffff',
            'text-valign': 'top',
            'background-opacity': 0,
            'color': '#000',
            # 'text-rotation': 'data(angle)',
        }
    },
    {
        'selector': '.unlinked',
        'style': {
            'opacity': 0.05,
            'z-index': 0
        }
    },
    {
        'selector': '.linked',
        'style': {
            'background-color': '#a2d5f2',
            'opacity': 1,
            'font-weight': '500',
            'border-width': '1px',
            'border-color': '#153b63',
            'z-index': 999
        }
    },
    {
        'selector': ':selected',
        'style': {
            'background-color': '#ff7e67',
            'border-width': '1px',
            'border-color': '#153b63',
            'z-index': 999
        }
    },
    {
        'selector': 'edge.linked',
        'style': {
            'width': '2px',
            'opacity': '1',
            'line-color': '#153b63',
            'z-index': 100
        }
    },
    {
        'selector': '.manager.linked',
        'style': {
            'width': '3px',
            'line-color': '#eecc44',
            'opacity': '1',
        }
    },
]
